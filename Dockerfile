FROM alpine:3.8

ARG INVENTORY_NAME

RUN echo "===> Installing sudo to emulate normal OS behavior"  && \
    apk --update add sudo                                         && \
    \
    \
    echo "===> Adding Python runtime"  && \
    apk --update add python py-pip openssl ca-certificates docker docker-py   && \
    apk --update add --virtual build-dependencies \
                python-dev libffi-dev openssl-dev build-base  && \
    pip install --upgrade pip cffi                            && \
    \
    \
    echo "===> Installing Ansible"  && \
    pip install ansible                && \
    \
    \
    echo "===> Installing additional tools"  && \
    pip install --upgrade pycrypto pywinrm && \
    apk --update add sshpass openssh-client rsync && \
    \
    \
    echo "===> Removing package list"  && \
    apk del build-dependencies            && \
    rm -rf /var/cache/apk/*
    
# Copy playbooks, inventory and ssh configuration
WORKDIR /playbooks
COPY integration/ops-import-docker-image.yml .
COPY integration/ops-import-helm-chart.yml .
COPY integration/ci-deploy-helm-chart.yml .
COPY inventory/${INVENTORY_NAME} ./inventory/${INVENTORY_NAME}
COPY ssh.${INVENTORY_NAME}.cfg .

RUN chmod 600 /playbooks/inventory/${INVENTORY_NAME}/*.key

# Create environment variables
ENV ANSIBLE_INVENTORY=inventory/${INVENTORY_NAME}/hosts.ini
ENV ANSIBLE_SSH_ARGS="-F ssh.${INVENTORY_NAME}.cfg"

CMD [ "/bin/sh" ]