# Grafana useful requests for Kafka monitoring

## Messages IN per sec by Pod
```
sum without(instance)(cp_kafka_server_brokertopicmetrics_messagesinpersec{job="kafka-backbone-cp-kafka"})
```