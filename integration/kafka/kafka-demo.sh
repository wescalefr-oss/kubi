#!/usr/bin/env bash
## Setup
export ZOOKEEPERS=kafka-backbone-cp-zookeeper:2181
export KAFKAS=kafka-backbone-cp-kafka-headless:9092

export TOPIC_NAME=demo-sprint
## Create Topic

kafka-topics --zookeeper kafka-backbone-cp-zookeeper:2181 --create --topic beta-topic --partitions 6 --replication-factor 1
kafka-topics --zookeeper $ZOOKEEPERS --create --topic $TOPIC_NAME --partitions 6 --replication-factor 1

## Producer
kafka-run-class org.apache.kafka.tools.ProducerPerformance --print-metrics --topic $TOPIC_NAME --num-records 1000000 --throughput 100000 --record-size 100 --producer-props bootstrap.servers=$KAFKAS buffer.memory=67108864 batch.size=8196

## Consumer
kafka-consumer-perf-test --broker-list $KAFKAS --messages 6000000 --threads 1 --topic $TOPIC_NAME --print-metrics

message size 1024