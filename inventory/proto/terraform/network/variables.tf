variable "aws_region" {
  type = "string"
  default = "eu-west-1"
}

variable "vpc_cidr" {
  type = "string"
  default = "10.43.0.0/16"
}

provider "aws" {
  region = "${var.aws_region}"
}