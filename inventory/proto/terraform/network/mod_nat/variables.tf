variable deploy_env {
  type = "string"
}

variable deploy_region {
  type = "string"
}

variable subnet_id_a {
  type = "string"
}

variable subnet_id_b {
  type = "string"
}

variable subnet_id_c {
  type = "string"
}

variable route_table_id_a {
  type = "string"
}

variable route_table_id_b {
  type = "string"
}

variable route_table_id_c {
  type = "string"
}
