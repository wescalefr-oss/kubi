variable "vpc_cidr" {}

variable "region" {}

variable "az_list" {
  type    = "list"
  default = []
}

variable "environment" {}
variable "owner" {
  default = "proto"
}
variable "stack" {}
variable "cost" {}
