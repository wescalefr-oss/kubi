module "vpc_layout" {
  source = "./mod_multi_az_vpc"

  vpc_cidr = "${var.vpc_cidr}"

  region = "${var.aws_region}"

  environment = "proto"
  stack       = "network-landscape"
  cost        = "global"
}

module "nat_layout" {
  source            = "./mod_nat"
  deploy_env        = "demo"
  subnet_id_a       = "${module.vpc_layout.public_subnet_id_a}"
  subnet_id_b       = "${module.vpc_layout.public_subnet_id_b}"
  subnet_id_c       = "${module.vpc_layout.public_subnet_id_c}"
  route_table_id_a  = "${module.vpc_layout.private_subnet_route_table_id_a}"
  route_table_id_b  = "${module.vpc_layout.private_subnet_route_table_id_b}"
  route_table_id_c  = "${module.vpc_layout.private_subnet_route_table_id_c}"
  deploy_region     = "${var.aws_region}"
}